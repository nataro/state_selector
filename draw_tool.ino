

/*
 太い線で四角枠を描画する
*/
void DrawWideLineRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
    tft.drawRect(x, y, w, h, color);
    tft.drawRect(x - 1, y - 1, w + 2, h + 2, color);
    tft.drawRect(x + 1, y + 1, w - 2, h - 2, color);
}

/*
 ボタンの枠をアクティブ状態用の色で描画
 （選択されているボタンの表示に使用）
*/
void MarkButton(ButtonContext *btn)
{
    uint16_t x = btn->pos_x;
    uint16_t y = btn->pos_y;
    uint16_t w = btn->width;
    uint16_t h = btn->height;
    uint16_t c_active = btn->color_active;

    DrawWideLineRect(x, y, w, h, c_active);
}

/*
 特定の位置にビットマップ画像を表示
 （選択されている状態の表示に使用）
*/
void MarkStateImage(ButtonContext *btn)
{
    // 画像貼り付け位置を指定
    uint16_t x = 10;
    uint16_t y = 0;
    uint16_t w = btn->width;
    uint16_t h = btn->height;
    uint16_t c_active = btn->color_active;
    const unsigned char *img = btn->img;

    tft.drawBitmap(x, y, img, w, h, c_active);
}

/*
 ButtonContext menu[] で指定される複数のボタンの枠をまとめて描画する
*/
void DrawButtonGroupFrame(ButtonContext *menu)
{
    for (uint16_t i = 1; menu->color_normal != 0; i++, menu++)
    {
        uint16_t x = menu->pos_x;
        uint16_t y = menu->pos_y;
        uint16_t w = menu->width;
        uint16_t h = menu->height;
        uint16_t c_normal = menu->color_normal;

        // ボタン枠を描画
        DrawWideLineRect(x, y, w, h, c_normal);

        // 選択中のボタン番号と一致する場合に下記処理を追加実行
        if (current_btn_num == i)
        {
            MarkButton(menu);
            // 表示テキストの更新
            String s = String(current_btn_num) + " is selected";
            DrawTextPlate(100, 5, 215, 45, s, 2);
        }
    }
}

/*
 ButtonContext menu[] で指定される複数のボタンの画像をまとめて描画する
*/
void DrawButtonGroupImage(ButtonContext *menu)
{
    for (; menu->color_normal != 0; menu++)
    {
        uint16_t x = menu->pos_x;
        uint16_t y = menu->pos_y;
        uint16_t w = menu->width;
        uint16_t h = menu->height;
        uint16_t c_normal = menu->color_normal;
        const unsigned char *img = menu->img;

        // ボタン枠を描画
        DrawWideLineRect(x, y, w, h, c_normal);
        // 画像を張り付ける（張り付ける時の色は黒で固定）
        tft.drawBitmap(x, y, img, w, h, ILI9341_BLACK);
    }
}

/*
 タッチセンサから得た座標を int16_t touch_x, int16_t touch_y として与え、その座標に該当するボタンの番号を取得
 ボタン番号「0」はボタンを選択していない状態とする
*/
uint16_t GetButtonNumber(ButtonContext *menu, int16_t touch_x, int16_t touch_y)
{
    for (uint16_t i = 1; menu->color_normal != 0; i++, menu++)
    {
        uint16_t x_min = menu->pos_x;
        uint16_t y_min = menu->pos_y;
        uint16_t x_max = x_min + menu->width;
        uint16_t y_max = y_min + menu->height;
        if ((x_min < touch_x) && (touch_x < x_max))
        {
            if ((y_min < touch_y) && (touch_y < y_max))
            {
                return i;
            }
        }
    }
    return 0; // ボタン非選択状態
}

/*
 uint16_t button_numで指定した番号のボタンが保持している列挙型（MainState）を取得
 （この関数を使って取得した列挙型を遷移先の状態値として利用する）
*/
MainState GetNextMainState(ButtonContext *menu, uint16_t button_num)
{
    for (uint16_t i = 0; i < button_num - 1; i++)
    {
        menu++;
    }
    // Serial.print("next->");
    // Serial.println(menu->next);
    return menu->next;
}

/*
 テキストを指定した位置に枠付きで描画
*/
void DrawTextPlate(uint16_t x, uint16_t y, uint16_t w, uint16_t h, String s, uint16_t size)
{
    uint16_t bg_color = WHITE;
    uint16_t txt_color = BLUE;
    uint16_t border_color = GRAY;

    // 枠の真ん中に文字が表示されるように調整
    int16_t dot_x = 6;
    int16_t dot_y = 8;
    int16_t val_x = ((int16_t)w - ((int16_t)size * dot_x) * ((int16_t)s.length())) * 0.5;
    int16_t val_y = ((int16_t)h - (int16_t)size * dot_y) * 0.5;
    int16_t offset_x = val_x < 0 ? 0 : val_x;
    int16_t offset_y = val_y < 0 ? 0 : val_y;

    // 枠を描画
    tft.fillRect(x, y, w, h, bg_color);
    tft.drawRect(x, y, w, h, border_color);

    // 文字を描画
    tft.setCursor(x + offset_x, y + offset_y);
    tft.setTextColor(txt_color);
    tft.setTextSize(size);
    tft.println(s.c_str());
}


/*
現在の選択状態に関する情報を画面に描画する
*/
void DrawCurrentInfomation()
{
    // 表示テキストの更新
    if (current_btn_num == 0)
    {
        DrawTextPlate(20, 10, 280, 40, "Unselected", 2);
    }
    else
    {
        uint16_t index = max(0, (uint16_t)current_main_state - 1 );
        MarkStateImage(&menu_arr[0][index]);
        DrawTabBorder(BLUE);
    }
}

/*
 タブみたいな感じの枠を描画する
*/
void DrawTabBorder(uint16_t color)
{

    tft.drawLine(2, 55, 5, 2, color);
    tft.drawLine(5, 2, 87, 2, color);
    tft.drawLine(87, 2, 90, 55, color);
    tft.drawLine(90, 55, 317, 55, color);
    tft.drawLine(317, 55, 317, 237, color);
    tft.drawLine(317, 237, 2, 237, color);
    tft.drawLine(2, 237, 2, 55, color);
}
