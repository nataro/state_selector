
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <Adafruit_STMPE610.h>

//ArduinoUNOはフラッシュメモリ不足でコンパイルできないと思います。MEGA推奨。
#include "bmp_data.h"

// // Color definitions
// // https://learn.adafruit.com/2-8-tft-touchscreen/graphics-library
#define BLACK 0x0000
#define BLUE 0x001F
#define RED 0xF800
#define GREEN 0x07E0
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define WHITE 0xFFFF
#define GRAY 0xC618 ///< 198, 195, 198 LightGray

// タッチパネルのキャリブレーションで用いる
// 【参考】file -> example sketch -> Adafruit ILI9341 -> touchpaint
#define TS_MINX 150
#define TS_MINY 130
#define TS_MAXX 3800
#define TS_MAXY 4000

#define STMPE_CS 8
Adafruit_STMPE610 ts = Adafruit_STMPE610(STMPE_CS);

#define TFT_CS 10
#define TFT_DC 9
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

// 描画するボタンの位置や大きさ
#define BTN_W 70 // ボタン群の幅
#define BTN_H 55 // ボタン群の高さ
#define GRID_X 13 // ボタン群の描画開始位置 X座標
#define GRID_Y 60 // ボタン群の描画開始位置 Y座標
#define SP_X 5    // ボタンとボタンの間隔 X方向
#define SP_Y 5    // ボタンとボタンの間隔 Y方向

#define UNTOUCH_TOLERANCE 25

typedef enum
{
  HOME,
  MAIN_01,
  MAIN_02,
  MAIN_03,
  MAIN_04,
  MAIN_05,
  MAIN_06,
  MAIN_07,
  MAIN_08,
  MAIN_09,
  MAIN_10,
  MAIN_11,
  MAIN_12
} MainState;

struct ButtonContext
{ // ボタン制御用の構造体
  uint16_t pos_x, pos_y;
  uint16_t width, height;
  uint16_t color_normal, color_active;
  const unsigned char *img;
  MainState next;
  // void (*on_click)(ButtonContext* ctx); // クリック時に実行
};


ButtonContext menu_arr[][13] = {
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_01 , MAIN_01},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_02 , MAIN_02},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_03 , MAIN_03},
    { GRID_X + ((SP_X + BTN_W) * 3), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_04 , MAIN_04},
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 1), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_05 , MAIN_05},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 1), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_06 , MAIN_06},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 1), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_07 , MAIN_07},
    { GRID_X + ((SP_X + BTN_W) * 3), GRID_Y + ((SP_Y + BTN_H) * 1), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_08 , MAIN_08},
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 2), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_09 , MAIN_09},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 2), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_10 , MAIN_10},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 2), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_11 , MAIN_11},
    { GRID_X + ((SP_X + BTN_W) * 3), GRID_Y + ((SP_Y + BTN_H) * 2), BTN_W, BTN_H, GRAY, BLUE,  bmp_main_12 , MAIN_12},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_01},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_01},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_01},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_02},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_02},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_02},
    { GRID_X + ((SP_X + BTN_W) * 3), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_04 , MAIN_02},
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 1), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_05 , MAIN_02},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_03},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_03},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_03},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_04},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_04},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_04},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_05},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_05},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_05},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_06},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_06},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_06},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_07},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_07},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_07},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_08},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_08},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_08},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_09},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_09},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_09},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_10},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_10},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_10},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_11},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_11},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_11},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  },
  {
    { GRID_X + ((SP_X + BTN_W) * 0), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_01 , MAIN_12},
    { GRID_X + ((SP_X + BTN_W) * 1), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_02 , MAIN_12},
    { GRID_X + ((SP_X + BTN_W) * 2), GRID_Y + ((SP_Y + BTN_H) * 0), BTN_W, BTN_H, GRAY, BLUE,  bmp_sub_03 , MAIN_12},
    { 0, 0, 0, 0, 0, 0, nullptr, 0}
  }
};

//// 下記「current_main_state」と「current_btn_num」で現在の状態を表現できる
//// 注意事項：「current_btn_num=0」は非選択状態を示している

// 現在のMainState
MainState current_main_state;

// 選択されたボタンの番号
uint16_t current_btn_num;

// 現在のメニュー表示の設定内容
ButtonContext *current_menu;

uint16_t touched_x; // タッチした座標Xを保存しておく
uint16_t touched_y; // タッチした座標Yを保存しておく

void setup()
{

  Serial.begin(9600); // シリアル通信の開始
  tft.begin();        // タッチスクリーンの開始
  if (!ts.begin())
  { // タッチスクリーンの開始
    Serial.println("Unable to start touchscreen.");
  }
  else
  {
    Serial.println("Touchscreen started.");
  }

  tft.fillScreen(ILI9341_WHITE); // スクリーン全体を白に塗る
  tft.setRotation(1);            // 左上を原点とする方向に画面の向きを設定

  current_main_state = HOME;
  current_btn_num = 0; //選択ボタンの初期化
  current_menu = menu_arr[(int)current_main_state];

  // 画面に描画
  DrawButtonGroupImage(current_menu);
  DrawButtonGroupFrame(current_menu);
  DrawCurrentInfomation();
}

void loop()
{

  if (!ts.bufferEmpty())
  { // タッチセンサにデータのバッファがある＝タッチしたとき
    // Retrieve a point
    TS_Point p = ts.getPoint();
    p.x = map(p.x, TS_MINY, TS_MAXY, 0, tft.height());
    p.y = map(p.y, TS_MINX, TS_MAXX, 0, tft.width());
    int y = tft.height() - p.x;
    int x = p.y;

    uint16_t btn_num = current_btn_num;

    // 2重に反応するのを防止するために、タッチした座標が前回の位置と異なる場合のみ処理する
    // 最後にタッチした座標と今回タッチしている座標の距離を計算
    float dist = sqrt(pow((float)x - (float)touched_x, 2) + pow((float)y - (float)touched_y, 2));
    if (dist > UNTOUCH_TOLERANCE)
    {
      current_menu = menu_arr[(uint16_t)current_main_state];
      btn_num = GetButtonNumber(current_menu, x, y);

      touched_x = x;
      touched_y = y;
    }

    if (btn_num != current_btn_num)
    {
      current_btn_num = btn_num;

      DrawButtonGroupFrame(current_menu);

      MainState state = GetNextMainState(current_menu, current_btn_num);
      if (state != current_main_state)
      {
        current_main_state = state;
        // HOMEに戻った時だけボタンを非選択状態「current_btn_num=0」にする
        current_btn_num = (current_main_state == HOME) ? 0 : 1;
        current_menu = menu_arr[(uint16_t)current_main_state];

        tft.fillScreen(ILI9341_WHITE); // スクリーン全体を白に塗る
        DrawButtonGroupImage(current_menu);
        DrawButtonGroupFrame(current_menu);
        DrawCurrentInfomation();
      }
    }
  }
  else
  { // if (!ts.bufferEmpty())
    //タッチパネルにデータのバッファが無い＝タッチしていない
    touched_x = 0;
    touched_y = 0;
  }
}
